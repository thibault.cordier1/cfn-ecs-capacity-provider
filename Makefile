.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


zip: ## Zip main.go to
	@rm -fr package
	@pip install --target ./package requests
	@cd package && zip -r9 ${OLDPWD}/function.zip . && cd ${OLDPWD}
	@zip -g function.zip lambda_function.py

upload-lambda: zip ## publish lambda to s3
	@aws s3 cp function.zip s3://devops-lambda-storage/ecs-capacity-provider.zip --acl public-read --profile default-perso

update-lambda: upload-lambda ## Update Lambda from S3
	@aws lambda update-function-code --function-name cfn-ECSCapacityProvider --zip-file fileb://function.zip --profile default-perso
