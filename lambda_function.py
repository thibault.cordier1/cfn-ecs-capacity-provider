import json
import logging
import random
import string

import boto3
import requests

logger = logging.getLogger(__name__)

try:
    ecs_client = boto3.client('ecs')
except Exception as e:
    logger.critical(e)


def get_autoscaling_group_arn(autoscaling_short_name):
    autoscaling_client = boto3.client('autoscaling')
    response = autoscaling_client.describe_auto_scaling_groups(
        AutoScalingGroupNames=[
            autoscaling_short_name,
        ],
        MaxRecords=1
    )
    if len(response['AutoScalingGroups']) == 1:
        print(json.dumps(response['AutoScalingGroups'][0]['AutoScalingGroupARN']))
        return response['AutoScalingGroups'][0]['AutoScalingGroupARN']
    else:
        raise AutoScalingGroupNotFoundException("Autoscaling group {} not found".format(autoscaling_short_name))


def create_capacity_provider(cluster_arn, capacity_provider_name, autoscaling_group_arn):
    logger.info("Creating capacity provider: {} on Autoscaling Group Arn: {}".format(capacity_provider_name,
                                                                                     autoscaling_group_arn))
    try:
        response = ecs_client.create_capacity_provider(
            name=capacity_provider_name,
            autoScalingGroupProvider={
                'autoScalingGroupArn': autoscaling_group_arn,
                'managedScaling': {
                    'status': 'ENABLED',
                    'targetCapacity': 100,
                    'minimumScalingStepSize': 1,
                    'maximumScalingStepSize': 1
                },
                'managedTerminationProtection': 'DISABLED'
            })
        if 'errorType' in response:
            raise CapacityProviderAssociationException(response['errorMessage'])

        response = ecs_client.put_cluster_capacity_providers(
            cluster=cluster_arn,
            capacityProviders=[
                capacity_provider_name,
            ],
            defaultCapacityProviderStrategy=[
                {
                    'capacityProvider': capacity_provider_name,
                    'weight': 1,
                    'base': 1
                },
            ]
        )
    except Exception as e:
        raise CapacityProviderAssociationException(e.__str__())
    return True


class InvalidCloudFormationParametersException(Exception):
    pass


class AutoScalingGroupNotFoundException(Exception):
    pass


class CapacityProviderAssociationException(Exception):
    pass


def sendresponse(event, context, responsestatus, responsedata, reason):
    """Send a Success or Failure event back to CFN stack"""
    payload = {
        'StackId': event['StackId'],
        'Status': responsestatus,
        'Reason': reason,
        'RequestId': event['RequestId'],
        'LogicalResourceId': event['LogicalResourceId'],
        'PhysicalResourceId': event['LogicalResourceId'] + \
                              ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) \
                                      for _ in range(10)),
        'Data': responsedata
    }
    print("Sending %s to %s" % (json.dumps(payload), event['ResponseURL']))
    requests.put(event['ResponseURL'], data=json.dumps(payload))
    print("Sent %s to %s" % (json.dumps(payload), event['ResponseURL']))


def lambda_handler(event, context):
    success = True
    reason = ""

    if event['RequestType'] == "Delete":
        success = True
        reason = "Delete is always a success"

    try:

        if event['RequestType'] == "Create":
            capacity_provider_name = event['LogicalResourceId'] + \
                                     ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) \
                                             for _ in range(10))

            if 'cluster_arn' in event['ResourceProperties']:
                cluster_arn = event['ResourceProperties']['cluster_arn']
            else:
                raise InvalidCloudFormationParametersException('cluster_arn not provived')

            if 'autoscaling_group_short_name' in event['ResourceProperties']:
                autoscaling_group_arn = get_autoscaling_group_arn(
                    event['ResourceProperties']['autoscaling_group_short_name'])
            else:
                raise InvalidCloudFormationParametersException('autoscaling_group_short_name not provived')

            create_capacity_provider(cluster_arn, capacity_provider_name, autoscaling_group_arn)

    except InvalidCloudFormationParametersException as e:
        success = False
        reason = e.__str__()

    except AutoScalingGroupNotFoundException as e:
        success = False
        reason = e.__str__()

    except CapacityProviderAssociationException as e:
        success = False
        reason = e.__str__()

    responsedata = {}
    try:

        if success:
            sendresponse(event, context, 'SUCCESS', responsedata, reason)
        else:
            sendresponse(event, context, 'FAILED', responsedata, reason)
    except Exception as e:
        raise Exception(e.__str__())